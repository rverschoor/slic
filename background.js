"use strict";

function onError(error) {
  console.error(`Slic Error: ${error}`);
}

// If browser action button is clicked, send a
// message to the active tab in the current window
chrome.browserAction.onClicked.addListener( () => {
  chrome.tabs.query({
		currentWindow: true,
		active: true
	}, (tab) => {
    if (chrome.runtime.lastError) {
      console.warn("addListener error: " + chrome.runtime.lastError.message);
    } else {
      chrome.tabs.sendMessage(tab[0].id, { 
          sender: "slic",
          request: "scrape", 
          id: tab[0].id 
        }, (response) => { 
          if (chrome.runtime.lastError) {
            console.warn("sendMessage error: " + chrome.runtime.lastError.message);
          } else {
            if (response) { 
              handleResponse(response);
            } 
          }
        } 
      );
    }
  });
});


function showNotification(tabid, type) {
  chrome.tabs.sendMessage(
    tabid,
    {sender: "slic", request: "notification", id: type}
  );
}

function handleResponse(response) {
  switch (response.data.page) {
    case "LicenseApp":
      // console.log("Triggered on LicenseApp page");
      composeText("template-license-app", response.data);
      break;
    case "CustomersPortalShow":
      // console.log("Triggered on CustomersPortalShow page");
      composeText("template-cp-show", response.data);
      break;
    case "CustomersPortalEdit":
      // console.log("Triggered on CustomersPortalEdit page");
      composeText("template-cp-edit", response.data);
      break;
    case "CustomersPortalImpersonate":
      // console.log("Triggered on CustomersPortalImpersonate page");
      composeText("template-cp-impersonate", response.data);
      break;
    case "Test":
      // console.log("Triggered on Test page");
      composeText("template-test-page", response.data);
      break;
    default:
      // console.log("Triggered on unknown page");
  }
}

function renderTemplate(name, template, info) {
  if (!template) {
    template = DEFAULT_TEMPLATE[name];
  }
  Mustache.escape = (text) => {return text;};
  copy(Mustache.render(template, info), info);
}

function composeText(name, info) {
  const getStoredTemplates = chrome.storage.local.get(name, (storage) => { 
    renderTemplate(name, storage[name], info);
  });
}

function copy(text, info) {
  chrome.tabs.sendMessage(
    info.tabid,
    {sender: "slic", request: "clipboard", clipboard: text}
  );
}
