"use strict";

chrome.runtime.onMessage.addListener(handleMessage);

function handleMessage (message, sender, sendResponse) {
  if (message.request === "scrape") {
    let info = collectInfo(message.id);
    sendResponse({ data: info });  
  } else if (message.request === "clipboard") {
    navigator.clipboard.writeText(message.clipboard).then( () => {
        notifySuccess("Copy successful");
      }, () => {
        notifyError("Copy failed");
      });
  }
  return true;
};

function notifyError(message) {
  alertify.error(message, 3, () => { console.log(message); });
}

function notifySuccess(message) {
  alertify.success(message, 3);
}

function collectInfo(tabid) {
  let info = {};
  info.tabid = tabid;
  let url = document.URL;
  info.url = url;

  // Impersonate = https://customers.gitlab.com/subscriptions
  if (url === "https://customers.gitlab.com/subscriptions") {
    collectInfoCustomersPortalImpersonate(info);
  } else {
    // Show = https://customers.gitlab.com/admin/customer/000000
    // Edit = https://customers.gitlab.com/admin/customer/000000/edit
    let match = url.match(
      /(https:\/\/customers.gitlab.com\/admin\/customer\/)(\d+)(.*)/
    );
    if (match && match.length == 4 && match[3] === "") {
      collectInfoCustomersPortalShow(info);
    } else if (match && match.length == 4 && match[3] === "/edit") {
      collectInfoCustomersPortalEdit(info);
    } else {
      info.page = "unknown";
      notifyError("Can't copy from this page");
    }
  }
  return info;
}

function collectInfoCustomersPortalShow(info) {
  // console.log("Collect CustomersPortalShow info");
  info.page = "CustomersPortalShow";

  // Details for Customer 'Jabberwocky'
  let head = document.querySelector("h1").textContent;
  let match = head.match(/(Details for Customer ')(.*)('$)/);
  if (match && match.length == 4) {
    info.customer = match[2];
  }

  let dt = document.querySelectorAll("div.content div.fieldset dl dt");
  let dd = document.querySelectorAll("div.content div.fieldset dl dd");
  if (dt.length == dd.length) {
    for (let i = 0; i < dt.length; i++) {
      // Field name, eg "First name" -> remove LF/CR, lowercase, snake_case
      let key = dt[i].textContent.trim().toLowerCase().replace(/ /g, "_");
      // Field value, eg "Jabberwocky" -> remove LF/CR
      let value = dd[i].textContent.trim();
      // console.log(`${key} = ${value}`);
      info[key] = value;
    }
  } else {
    notifyError("Error: dt and dd length not equal");
  }
}

function collectInfoCustomersPortalEdit(info) {
  // console.log("Collect CustomersPortalEdit info");
  info.page = "CustomersPortalEdit";

  // Edit Customer 'Jabberwocky'
  let head = document.querySelector("h1").textContent;
  let match = head.match(/(Edit Customer ')(.*)('$)/);
  if (match && match.length == 4) {
    info.customer = match[2];
  }
  
  let fields = document.querySelectorAll("div.content fieldset div.form-group");
  for (let field of fields) {
    let childs = field.childNodes;
    if (childs.length == 2) {
      // Form label, eg "First name" -> lowercase, snake_case
      let key = childs[0].textContent.toLowerCase().replace(/ /g, "_");
      // Input field
      let field_type = childs[0].control.type;
      let value = "???";
      switch (field_type) {
        case "text":
          //Text field: default value, eg "Jabberwocky" -> remove LF/CR
          value = childs[0].control.value.trim();
          break;
        case "checkbox":
          value = childs[0].control.value;
          break;
        default:
          notifyError(`Unknown fieldtype ${field_type}`);
      }
      // console.log(`${key} = ${value}`);
      info[key] = value;
    }
  }
}

// Nasties:
// https://customers.gitlab.com/admin/customer/29333/edit
// https://customers.gitlab.com/admin/customer/9669/edit
// https://customers.gitlab.com/admin/customer/16399/edit
function collectInfoCustomersPortalImpersonate(info) {
  // console.log("Collect CustomersPortalImpersonate info");
  info.page = "CustomersPortalImpersonate";

  let alert = document.querySelector("div.alert-warning").textContent.trim().split('\n');
  if (alert.length > 1) {
    info.alert = alert[1];
  }

  info.subs = [];
  let subscriptions = document.querySelectorAll("div.subscription-list div.collapse-container");
  for (const [i, subscription] of subscriptions.entries()) {
    let sub = {};
    let header = subscription.querySelector("h2");
    let title = header.textContent.trim();
    sub.title = title;
    let subinfo = header.nextElementSibling.children;
    sub.plan = subinfo[0].textContent.trim();
    sub.subscription = subinfo[1].textContent.trim();
    if (subinfo.length > 2) {
      sub.renewal = subinfo[2].textContent.trim() || '** no renewal date **';
    }
    sub.purchases = [];
    let rows = subscription.querySelectorAll(`div#collapse-container-id-${i} table tbody tr`);
    for (let row of rows) {
      let purchases = {};
      let fields = row.children;
      purchases.start_date = fields[0].textContent;
      purchases.purchase_date = fields[1].textContent;
      purchases.product = fields[2].textContent;
      purchases.quantity = fields[3].textContent;
      sub.purchases.push(purchases);
    }
    info.subs.push(sub);
  }
  // console.log(info);
}
