"use strict";

// https://github.com/janl/mustache.js/

const TEMPLATE_LICENSE_APP =
`URL = {{url}}
Name = {{name}}
Company = {{company}}
Email = {{email}}
Issued at = {{issued_at}}
Starts at = {{starts_at}}
Expires at = {{expires_at}}
LUC = {{users_count}}
PUC = {{previous_users_count}}
TUC = {{trueup_count}}
AddOns = {{add_ons}}
Zuora sub = {{zuora_subscription}}
Zuora name = {{zuora_subscription_name}}
Trial = {{trial}}
Notes = {{notes}}
Plan = {{gitlab_plan}}
Creator = {{creator}}
`;

const TEMPLATE_CUSTOMERS_PORTAL_SHOW = 
`URL = {{url}}
Customer = {{customer}}
First name = {{first_name}}
Last name = {{last_name}}
Email = {{email}}
Company = {{company}}
Zuora account = {{zuora_account}}
SF account = {{salesforce_account}}
Login activated = {{login_activated}}
EULA request = {{eula_request}}
`;

const TEMPLATE_CUSTOMERS_PORTAL_EDIT = 
`URL = {{url}}
Customer = {{customer}}
First name = {{first_name}}
Last name = {{last_name}}
Email = {{email}}
Zuora account = {{zuora_account}}
SF account = {{salesforce_account}}
Login activated = {{login_activated}}
`;

const TEMPLATE_CUSTOMERS_PORTAL_IMPERSONATE = 
`{{#alert}}
:warning: {{alert}}
{{/alert}}
Subscriptions:
{{#subs}}
- Title = {{title}}
  Plan = {{plan}}
  Subscription = {{subscription}}
  Renewal = {{renewal}}
  Purchases:
  {{#purchases}}
  - Start date = {{start_date}}
    Purchase date = {{purchase_date}}
    Product = {{product}}
    Quantity = {{quantity}}
  {{/purchases}}
  {{/subs}}
`;

const TEMPLATE_TEST_PAGE = 
`URL = {{url}}
{{#projects}}
- {{title}}
{{/projects}}
`;

const DEFAULT_TEMPLATE = {
  "template-license-app": TEMPLATE_LICENSE_APP,
  "template-cp-show": TEMPLATE_CUSTOMERS_PORTAL_SHOW,
  "template-cp-edit": TEMPLATE_CUSTOMERS_PORTAL_EDIT,
  "template-cp-impersonate": TEMPLATE_CUSTOMERS_PORTAL_IMPERSONATE,
  "template-test-page": TEMPLATE_TEST_PAGE
};

const PAGES = ["license-app", "cp-show", "cp-edit", "cp-impersonate", "test-page"];
